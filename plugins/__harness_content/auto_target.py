
from os import environ
from brain import connect
from brain.queries import insert_target
from brain.telemetry import get_target


PLUGIN_NAME = "Harness"


def auto_target(target_id):
    """
    Currently will pull all targets from Brain.Targets
    and verify if Brain knows about the target we are talking to.
    If target is not in Brain DB, this will add it with a Default (Init: UUID)
    Next add a ENV to turn on and off ability.
    :param target_id: the ID for the location field of the target
    :return: None
    """
    if not target_id:
        return "ERROR: No Agent UUID Provided"

    automatically_add_targets = environ.get("AUTO_TARGET", "auto_targets")
    if automatically_add_targets is "1":
        brain_conn = connect()
        target = get_target(PLUGIN_NAME, environ.get("PORT", "0"), target_id, conn=brain_conn)
        if not target:
            new_target = {"PluginName": PLUGIN_NAME,
                          "Location": target_id,
                          "Port": str(environ.get("PORT", "0")),
                          "Optional": {"init": target_id}}
            response = insert_target(new_target, verify_target=True, conn=brain_conn)
            print(f"Automatically added {PLUGIN_NAME} contact: {target_id}")
    else:
        print(f'Unsolicited Target from : {target_id}')
