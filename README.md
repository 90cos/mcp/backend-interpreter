# Plugin Interpreter

## Guide

**Building interpreter image**

From top level repository:

`docker build -t interpreter-plugin:<tag> .`

**Running interpreter by itself in detached mode (development)**

Create network and start database.

```
$ docker network create test
$ docker run -d --rm --name rethinkdb --network test -p 28015:28015 -e "STAGE=DEV" -e "LOGLEVEL=DEBUG" datbase-brain:<dev,qa,latest>
```

Run the controller in the network you created.
```
$ docker run --rm --name controller --network test -ti -p <external_port>:<plugin_port> -e "STAGE=PROD" -e "LOGLEVEL=DEBUG" -e "PLUGIN=<plugin_name>" -e "PORT=<plugin_port>" interpreter-plugin:<tag>
```