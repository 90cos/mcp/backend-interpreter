FROM alpine:3.7

RUN apk update && \
    apk upgrade && \
    apk add --update bash py-pip python3 && \
    rm -rf /var/cache/apk/*



WORKDIR /tmp
COPY ./requirements.txt .

RUN pip3 install --extra-index-url https://MCP-Package-Repo:${PIP_ACCESS_TOKEN}@gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain && \
    pip3 install -r requirements.txt

WORKDIR /opt/app-root/src
COPY . .

HEALTHCHECK --interval=2s --start-period=10s --retries=3 CMD /bin/sh ./healthcheck.sh

ENTRYPOINT python3 server.py "${PLUGIN}"
